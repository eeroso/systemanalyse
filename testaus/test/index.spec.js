const supertest = require("supertest");
const assert = require('assert');
const app = require("../server");
 /**describe("GET /", function() {
 it("it should has status code 200", function(done) {
 supertest(app)
 .get("/")
 .expect(200)
 .end(function(err, res){
    if (err) done(err);
        done();
        });
    });
    it("it shoud has response with hope key with value of loop", function(done){
        supertest(app)
        .get("/")
        .expect({ hope: "loop" })
        .end(function(err, res){
        if (err) done(err);
        done();
        });
       });

});*/

describe("POST /", function(){
    it("palauttaa 'true' jos kirjautuminen onnistuu", function(done) {
    supertest(app)
    .post("/")
    .send({ username: "admin" , password: "admin" })
    .expect('true')
    .end(function(err, res){
        if (err) done(err);
        done();
    });
    });

    it("palauttaa 'Incorrect Username and/or Password!', jos kayttajatunnus tai salasana vaarin", function(done){
        supertest(app)
        .post("/")
        .send({username: "admin" , password: "admins"})
        .expect('Incorrect Username and/or Password!')
        .end(function(err, res){
        if (err) done(err);
        done();
        });
        });
    });
   


